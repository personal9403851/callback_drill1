const fs = require('fs');
const path = require('path');

const fsProblem1 = (absolutePathOfRandomDirectory, randomNumberOfFiles) => {

    let dirPath = absolutePathOfRandomDirectory;
    let numberOfFiles = randomNumberOfFiles;
    let filesWritten = 0;

    const createDirectory = (cbCreateJson) => {

        fs.mkdir(dirPath, (error) => {
            if (error) {
                console.log('Error Creating File' + error);
            } else {
                console.log(`Directory created successfully`);
            }
        });
        cbCreateJson(deleteFilesSimultaneously);
    };

    const createRandomJsonFiles = (cbDelete) => {

        for (let index = 0; index < numberOfFiles; index++) {
            const filePath = path.join(dirPath, `RandomFile${index}.json`);
            const jsonData = { key: `value${index}` };

            fs.writeFile(filePath, JSON.stringify(jsonData), (error) => {
                if (error) {
                    console.log(`Error writing into the json file: ${error}`);
                } else {
                    console.log(`Json File created successfully: ${filePath}`);
                    filesWritten++;

                    // Check if all files have been written before calling cbDelete
                    if (filesWritten === numberOfFiles) {
                        cbDelete();
                    }
                }
            });
        }
    };

    const deleteFilesSimultaneously = () => {

        for (let index = 0; index < numberOfFiles; index++) {
            const filePath = path.join(dirPath, `RandomFile${index}.json`);

            fs.unlink(filePath, (error) => {
                if (error) {
                    console.log(`Error deleting the json file: ${filePath}`);
                } else {
                    console.log(`Json file deleted successfully: ${filePath}`);
                }
            });
        }
    };

    createDirectory(createRandomJsonFiles);
};

module.exports = fsProblem1;

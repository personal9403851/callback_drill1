const fs = require('fs')

const fsProblem2 = () => {
    let LipsumFileContent = '';

    const readFile = (callback) => {
        const filePath = '../lipsum_1.txt';

        LipsumFileContent = fs.readFile(filePath, 'utf8', (error, content) => {
            if (error) {
                console.log(`Error Reading File: ${error}`);
            } else {
                LipsumFileContent = content;
                console.log(`File Read Successfully`);
                callback(splitIntoSentences);
            }
        });
    }

    const convertToUpperCase = (callback) => {

        LipsumFileContent.toString()
        const convertedContent = LipsumFileContent.toUpperCase()

        const filePath = './upperCase.txt'
        fs.writeFile(filePath, convertedContent, (errorOuter) => {

            if (errorOuter) {
                console.log(`Error while writing the uppercase content: ${errorOuter}`)
            }
            else {
                fs.writeFile('./fileNames.txt', 'upperCase.txt\n', (errorInner) => {
                    if (errorInner) {
                        console.log(`Error writing into FileNames.txt: ${errorInner}`)
                    }
                    else {
                        console.log(`Name of the file written in FileNames.txt successfully`)
                    }
                })
            }
        })
        callback(sortNewFilesContent);
    }

    const splitIntoSentences = (callback) => {

        let data = ''

        fs.readFile('./upperCase.txt', 'utf8', (error, content) => {

            if (error) {
                console.log(`Error while reading Uppercase File: ${error}`)
            }
            else {
                data = content
                console.log(`Uppercase file read successfully`)
            }
        })

        let sentencesArray = []
        sentencesArray = data.split('.')

        for (let index = 0; index < sentencesArray.length; index++) {

            fs.appendFile('./fileInSentences.txt', `${sentencesArray[index]}\n`, (error) => {
                if (error) {
                    console.error(`Error writing sentence: ${error}`);
                } else {
                    console.log(`The content has been splitted into sentences and written into the file`)
                }
            })
        }

        fs.appendFile('./fileNames.txt', 'fileInSentences.txt\n', (error) => {
            if (error) {
                console.error(`Error adding name of the file into fileNames.txt: ${error}`);
            } else {
                console.log(`File name successfully added`)
            }
        })
        callback(deleteNewFiles);
    }

    const sortNewFilesContent = (callback) => {

        let fileNamesData = ''

        fs.readFile('./fileNames.txt', 'utf8', (error, content) => {
            if (error) {
                console.log(`Error reading file names: ${error}`)
            }
            else {
                console.log(`FileNames successfully read`)
                fileNamesData = content;
            }
        })

        let fileInSentencesData = '';
        fs.readFile('./fileInSentences.txt', 'utf8', (error, content) => {
            if (error) {
                console.log(`Error reading the fileInSentencesData: ${error}`)
            }
            else {
                fileInSentencesData = content;
                console.log(`Successfully read the fileInSentencesData`)
            }
        })

        let fileNamesArray = fileNamesData.split('\n')
        let fileInSentencesArray = fileInSentencesData.split('\n')

        fileNamesArray.sort((a, b) => {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        })

        fileInSentencesArray.sort((a, b) => {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        })

        for (let index = 0; index < fileNamesArray.length; index++) {

            fs.appendFile('./fileNamesSorted.txt', `${fileNamesArray[index]}\n`, (error) => {
                if (error) {
                    console.log(error)
                }
                else {
                    console.log(`Successfully written sorted FileNames`)
                }
            })
        }

        for (let index = 0; index < fileInSentencesArray.length; index++) {

            fs.appendFile('./fileInSentencesSorted.txt', `${fileInSentencesArray[index]}\n`, (error) => {
                if (error) {
                    console.log(error)
                }
                else {
                    console.log(`Successfully written sorted File Sentences`)
                }
            })
        }

        fs.appendFile('./fileNames.txt', 'fileInSentencesSorted.txt\n', (error) => {
            if (error) {
                console.log(error)
            }
            else {
                console.log(`sorted Sentences File successfully added to the fileNames.txt`)
            }
        })

        fs.appendFile('./fileNames.txt', 'fileNamesSorted.txt\n', (error) => {
            if (error) {
                console.log(error)
            }
            else {
                console.log(`Sorted Names File successfully added to the fileNames.txt`)
            }
        })
        callback();
    }

    const deleteNewFiles = () => {

        fs.writeFile('./fileNames.txt', '', (error) => {
            if (error) {
                console.log(error)
            }
            else {
                console.log(`Files successfully deleted`)
            }
        })
    }

    readFile(convertToUpperCase);
}

module.exports = fsProblem2;